In the folder datas/48_48/:
===========================

 Here are stored **all the pictures used for the game**.
 Those *png* pictures are used by the *client*, to build a *GUI* with *PyGame*.
 
 The *GUI* can be disable with the *--nowindow* option, or you can force to try to launch it with the *--window* option.
 The game should run without any additionnal window. But maybe it require an *X server* anyway.

A description:
--------------

 1. **block_destr.png**, **block_undestr.png**:
    For the wall (destructible or not).
 2. **player{0|1|2|3|4|5|6|7|8|9}.png** : 
    For the players (many different models).
 3. **bomb_{1|2|3|4}** : 
    For the bombs (many different models).

About the pictures:
=====================

 The soundtrack is from many different sources.
 This is a non exhaustive list.

 1. *Pacman* (**Nintendo (c) 1991**);
 2. *Super Mario World 1* (**Nintendo (c) 1985**);
 3. *Pokémon Red/Blue* (**Nintendo (c) 1995 - GameFreak**).
 
 Moreover, some images were made with an online generator [http://www.softicons.com/icon-tools/icon-converter], so you can also use this to make your own !
	
Copyrigths:
============

 The game is released under the *GPLv3 Licence*, but the soundtrack **is not** distributed freely with the game (at least, not under a free licence).
 Although, you can download it from the same source that the game (just not in the same archive).

